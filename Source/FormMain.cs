﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using mySqlLib;

namespace CSharp_project_01
{
    public partial class formMain : Form
    {
        MySqlLib mySqlLib;
        // переделать под SqlConnectionStringBuilder
        private string connectionString="Data Source=sql6001.smarterasp.net;Initial Catalog=DB_A1E1BD_scar4ace;Persist Security Info=True;User ID=DB_A1E1BD_scar4ace_admin;Password=My2245525pass";
        public formMain()
        {
            InitializeComponent();
            UpdateDataGridView();
        }
        public void UpdateDataGridView()
        {
            // Пример запроса mySqlLib.SqlReturnDataset()
            mySqlLib = new MySqlLib();
            DataTable sqlData = mySqlLib.SqlReturnDataset(@"SELECT Id,Manufacturer,Model,Country,Year,Description FROM ConceptCarsEntries;", connectionString);
            dataGridEntries.DataSource = sqlData.DefaultView;// Инициализация DataGridView таблицей
            dataGridEntries.Sort(dataGridEntries.Columns["Id"] ,ListSortDirection.Ascending);// Сортировка по колонке Id
            textBoxDescription.Text = "Select entry to view description";
        }
        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            ShowDescription();
        }
        private void ShowDescription()
        {
            if (dataGridEntries.CurrentRow!=null)
            {
                textBoxDescription.Text = dataGridEntries.CurrentRow.Cells["Description"].Value.ToString();
            }
        }
        private void buttonAddCar_Click(object sender, EventArgs e)
        {
            FormAddCar addForm = new FormAddCar();
            if(addForm.ShowDialog()==DialogResult.OK)
            {
                AddRowToSql(addForm);
            }
        }

        // Добавление записи
        private void AddRowToSql(FormAddCar _addForm)
        {
            int Id;
            if (mySqlLib.SqlScalar("SELECT MAX(Id) FROM ConceptCarsEntries", connectionString) != null)
            {
                int.TryParse(mySqlLib.SqlScalar("SELECT MAX(Id) FROM ConceptCarsEntries", connectionString),out Id);
                Id++;
            }
            else
                Id = 1;

            string query = string.Format(@"INSERT INTO ConceptCarsEntries (Id, Manufacturer, Model, Country, Year, Description) VALUES ({0}, '{1}', '{2}', '{3}', {4}, '{5}');", Id, _addForm.Manufacturer, _addForm.Model, _addForm.Country, _addForm.Year, _addForm.Description);
            mySqlLib.SqlNoneQuery(query, connectionString);
            UpdateDataGridView();
        }

        // Удаление записи
        private void buttonRemoveCar_Click(object sender, EventArgs e)
        {
            string query = string.Format("DELETE FROM ConceptCarsEntries WHERE Id={0}", dataGridEntries.CurrentRow.Cells["Id"].Value.ToString());
            mySqlLib.SqlNoneQuery(query,connectionString);
            UpdateDataGridView();
        }

        // Апдейт DataGridView
        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            UpdateDataGridView();
        }
    }
}
