﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CSharp_project_01
{
    public partial class FormAddCar : Form
    {
        public FormAddCar()
        {
            InitializeComponent();
        }

        private void pictureBoxBigPreview_Click(object sender, EventArgs e)
        {

        }

        private void FormAddCar_Load(object sender, EventArgs e)
        {

        }
        // Properties
        public string Manufacturer { get; set; }
        public string Model { get; set; }
        public string Country { get; set; }
        public int Year { get; set; }
        public string Description { get; set; }
        //public Image Preview { get; set; }

        private void buttonAddCar_Click(object sender, EventArgs e)
        {
            // Set values from User
            Manufacturer = comboBoxManufacturer.Text;
            Model = textBoxModel.Text;
            Country = comboBoxCountry.Text;
            int outYear;
            Int32.TryParse(textBoxReleaseDate.Text,out outYear);
            Year = outYear;
            Description = textBoxDescription.Text;
            //Preview = pictureBoxBigPreview.Image;
        }
    }
}
