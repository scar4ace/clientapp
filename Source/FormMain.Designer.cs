﻿namespace CSharp_project_01
{
    partial class formMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridEntries = new System.Windows.Forms.DataGridView();
            this.pictureBoxBigPreview = new System.Windows.Forms.PictureBox();
            this.textBoxDescription = new System.Windows.Forms.TextBox();
            this.buttonAddCar = new System.Windows.Forms.Button();
            this.buttonRemoveCar = new System.Windows.Forms.Button();
            this.buttonUpdate = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridEntries)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxBigPreview)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridEntries
            // 
            this.dataGridEntries.AllowUserToAddRows = false;
            this.dataGridEntries.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridEntries.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.dataGridEntries.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridEntries.GridColor = System.Drawing.SystemColors.ControlDarkDark;
            this.dataGridEntries.Location = new System.Drawing.Point(12, 198);
            this.dataGridEntries.Name = "dataGridEntries";
            this.dataGridEntries.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dataGridEntries.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black;
            this.dataGridEntries.RowTemplate.ReadOnly = true;
            this.dataGridEntries.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridEntries.Size = new System.Drawing.Size(543, 179);
            this.dataGridEntries.TabIndex = 0;
            this.dataGridEntries.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            // 
            // pictureBoxBigPreview
            // 
            this.pictureBoxBigPreview.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBoxBigPreview.Location = new System.Drawing.Point(12, 12);
            this.pictureBoxBigPreview.Name = "pictureBoxBigPreview";
            this.pictureBoxBigPreview.Size = new System.Drawing.Size(240, 180);
            this.pictureBoxBigPreview.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBoxBigPreview.TabIndex = 3;
            this.pictureBoxBigPreview.TabStop = false;
            // 
            // textBoxDescription
            // 
            this.textBoxDescription.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxDescription.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.textBoxDescription.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxDescription.Location = new System.Drawing.Point(258, 12);
            this.textBoxDescription.Multiline = true;
            this.textBoxDescription.Name = "textBoxDescription";
            this.textBoxDescription.ReadOnly = true;
            this.textBoxDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxDescription.Size = new System.Drawing.Size(297, 180);
            this.textBoxDescription.TabIndex = 4;
            // 
            // buttonAddCar
            // 
            this.buttonAddCar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonAddCar.Location = new System.Drawing.Point(318, 383);
            this.buttonAddCar.Name = "buttonAddCar";
            this.buttonAddCar.Size = new System.Drawing.Size(75, 23);
            this.buttonAddCar.TabIndex = 1;
            this.buttonAddCar.Text = "Add car";
            this.buttonAddCar.UseVisualStyleBackColor = true;
            this.buttonAddCar.Click += new System.EventHandler(this.buttonAddCar_Click);
            // 
            // buttonRemoveCar
            // 
            this.buttonRemoveCar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonRemoveCar.Location = new System.Drawing.Point(399, 383);
            this.buttonRemoveCar.Name = "buttonRemoveCar";
            this.buttonRemoveCar.Size = new System.Drawing.Size(75, 23);
            this.buttonRemoveCar.TabIndex = 2;
            this.buttonRemoveCar.Text = "Remove Car";
            this.buttonRemoveCar.UseVisualStyleBackColor = true;
            this.buttonRemoveCar.Click += new System.EventHandler(this.buttonRemoveCar_Click);
            // 
            // buttonUpdate
            // 
            this.buttonUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonUpdate.Location = new System.Drawing.Point(480, 383);
            this.buttonUpdate.Name = "buttonUpdate";
            this.buttonUpdate.Size = new System.Drawing.Size(75, 23);
            this.buttonUpdate.TabIndex = 5;
            this.buttonUpdate.Text = "Update";
            this.buttonUpdate.UseVisualStyleBackColor = true;
            this.buttonUpdate.Click += new System.EventHandler(this.buttonUpdate_Click);
            // 
            // formMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.ClientSize = new System.Drawing.Size(567, 412);
            this.Controls.Add(this.buttonUpdate);
            this.Controls.Add(this.buttonRemoveCar);
            this.Controls.Add(this.buttonAddCar);
            this.Controls.Add(this.textBoxDescription);
            this.Controls.Add(this.pictureBoxBigPreview);
            this.Controls.Add(this.dataGridEntries);
            this.MinimumSize = new System.Drawing.Size(583, 293);
            this.Name = "formMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Concept cars catalogue";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridEntries)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxBigPreview)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridEntries;
        private System.Windows.Forms.PictureBox pictureBoxBigPreview;
        private System.Windows.Forms.TextBox textBoxDescription;
        private System.Windows.Forms.Button buttonAddCar;
        private System.Windows.Forms.Button buttonRemoveCar;
        private System.Windows.Forms.Button buttonUpdate;
    }
}

